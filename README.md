## WPezClasses: Theme Add Support

__WordPress add_theme_support() done The ezWay. Cleaned up and consolidated for ez reference and usage.__
   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\ThemeAddSupport\ClassNThemeAddSupport as Register;
use WPezSuite\WPezClasses\ThemeAddSupport\ClassHooks as Hooks;

$arr_feats = [
    'f1' => [
        'active' => true,
        'feature' => 'post-formats',
        'args' => [...]
        ],
    'f1' => [
        'active' => true,
        'feature' => 'post-thumbnails',
        'args' => [...]
        ],
    ];
    
$arr_feat = [
        'active' => true,
        'loc' => 'header_mobile',
        'desc' => 'Header Mobile'
    ];
    
$new = new Register();
// load via array
$new->loadFeatures($arr_menus);
// or push one at a time
$new->pushFeature($arr_menu);
// or 
$arr_html5_args = [...];
$new->add('html5', $arr_html5_args);
// or use the individual methods
$this->addAlignWide();

// inject into the hooks    
$new_hooks = new Hooks($new);
// register...done!
$new_hooks->register();

```

### FAQ

__1) Why?__

Aside from the fact this will play nice with your IDE, it's the usual The ezWay pitch: configuring (arrays) is faster, easier and less stressful then writing and testing code.

__2) This is code and a reference documents all-in-one.__

Yup! Cleaned up, consolidated, and at your finger tips. You're welcome. 

__3) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 

 __4) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit.


### Helpful Links

- https://developer.wordpress.org/reference/functions/add_theme_support/

- https://developer.wordpress.org/block-editor/developers/themes/theme-support/

- https://make.wordpress.org/core/2016/11/30/starter-content-for-themes-in-4-7/


### TODO

n/a

### CHANGE LOG

- v0.0.2 - Monday 23 September 2019
    - FIXED - Bug in args can be array and args can be none

- v0.0.1 - Friday 13 September 2019
    - Hey! Ho!! Let's go!!! Yet another "finally doing a proper repo of this tool." Please pardon the delay.